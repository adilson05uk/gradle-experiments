# Useful commnands

Run task by specifying build file and task:

```./gradlew -b a/build.gradle a1```

Run task by specifying project name and task:

```./gradlew -p a a1```

```./gradlew a a1```

```./gradlew :a:a1```

Exclude task from execution

```./gradlew b4 -x b1```

Force run task

```./gradlew --rerun-tasks a1```

Create build scan

```./gradlew :a:a1 --scan```

List projects

```./gradlew :projects```

```./gradlew :a:projects```

List tasks

```./gradlew :tasks```

```./gradlew :a:tasks```

List all tasks (including hidden ones)

```./gradlew :tasks --all```

List properties

```./gradlew properties```

Show task info

```./gradlew help --task :a:a1```

